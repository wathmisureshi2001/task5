package FP_Experiments;
import java.util.*;
public class IPL04 {
  public static void main(String[] args) {
    List<Match> table = Arrays.asList(
        new Match(1304113,"Mumbai","67","Gujarat Titans","Royal Challengers Bangalore","Gujarat Titans",
            "bat","Royal Challengers Bangalore","Wickets",8,"V Kohli"),
        new Match(1304112,"Navi Mumbai","66","Lucknow Super Giants","Kolkata Knight Riders","Lucknow Super Giants",
            "bat","Lucknow Super Giants","Runs",2,"Q de Kock"),
        new Match(1304111,"Mumbai","65","Sunrisers Hyderabad","Mumbai Indians","Mumbai Indians","field","Sunrisers Hyderabad",
            "Runs",3,"RA Tripathi"),
        new Match(1304110,"Navi Mumbai","64","Delhi Capitals","Punjab Kings","Punjab Kings","field","Delhi Capitals","Runs",
            17,"SN Thakur"),
        new Match(1304109,"Mumbai","63","Rajasthan Royals","Lucknow Super Giants","Rajasthan Royals","bat","Rajasthan Royals",
            "Runs",24,"TA Boult"),
        new Match(1304108,"Mumbai","62","Chennai Super Kings","Gujarat Titans","Chennai Super Kings",
            "bat","Gujarat Titans","Wickets",7,"WP Saha"),
        new Match(1304107,"Pune","61","Kolkata Knight Riders","Sunrisers Hyderabad","Kolkata Knight Riders",
            "bat","Kolkata Knight Riders","Runs",54,"AD Russell"),
        new Match(1304106,"Mumbai","60","Punjab Kings","Royal Challengers Bangalore","Royal Challengers Bangalore","field",
            "Punjab Kings","Runs",54,"JM Bairstow"),
        new Match(1304105,"Mumbai","59","Chennai Super Kings","Mumbai Indians","Mumbai Indians","field","Mumbai Indians",
            "Wickets",5,"DR Sams"),
        new Match(1304104,"Navi Mumbai","58","Rajasthan Royals","Delhi Capitals","Delhi Capitals","field","Delhi Capitals",
            "Wickets",8,"MR Marsh"),
        new Match(1304103,"Pune","57","Gujarat Titans","Lucknow Super Giants","Gujarat Titans","bat",
            "Gujarat Titans","Runs",62,"Shubman Gill"),
        new Match(1304102,"Navi Mumbai","56","Kolkata Knight Riders","Mumbai Indians","Mumbai Indians","field","Kolkata Knight Riders",
            "Runs",52,"JJ Bumrah"));
    System.out.println("Matches won by Runs");
    table.stream().filter(club -> club.getWonBy() == "Runs")
        .forEach(System.out::println);

    System.out.println();
    System.out.println("Matches with margin at 7");
    table.stream().filter(club -> club.getMargin() == 7)
        .forEach(System.out::println);

  }
}
